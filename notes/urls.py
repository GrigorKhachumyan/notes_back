from django.urls import path
from .views import *

urlpatterns =[
    path('project/<str:pk>', ProjectAPIView.as_view(), name='project'),
    path('project', ProjectAPIView.as_view(), name='project'),

    path('link/<str:pk>', LinkAPIView.as_view(), name='link'),
    path('link', LinkAPIView.as_view(), name='link'),

    path('information/<str:pk>', InformationAPIView.as_view(), name='information'),
    path('information', InformationAPIView.as_view(), name='information'),

    # path('application/<str:pk>', ApplicationAPIView.as_view(), name='application'),
    # path('application/', ApplicationAPIView.as_view(), name='application'),
]