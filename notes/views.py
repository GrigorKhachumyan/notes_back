from rest_framework import generics

from .serializers import *


class ProjectAPIView(generics.ListCreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class LinkAPIView(generics.ListCreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    queryset = Link.objects.all()
    serializer_class = LinkSerializer

    def get_queryset(self) -> dict:
        qs = self.queryset.all()
        q = self.request.GET.get('project', )
        if q:
            qs = qs.filter(project=q)

        return qs


class InformationAPIView(generics.ListCreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    queryset = Information.objects.all()
    serializer_class = InformationSerializer

    def get_queryset(self) -> dict:
        qs = self.queryset.all()
        q = self.request.GET.get('project', )
        if q:
            qs = qs.filter(project=q)

        return qs
