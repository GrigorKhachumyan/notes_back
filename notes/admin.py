from django.contrib import admin
from notes.models import (
    Project,
    Link,
    Information
)


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    ordering = ('id',)

    class Meta:
        model = Project


class LinkAdmin(admin.ModelAdmin):
    list_display = ('id', 'project', 'link',)
    ordering = ('id',)

    class Meta:
        model = Link


class InformationAdmin(admin.ModelAdmin):
    list_display = ('id', 'project', 'mail', 'password',)
    ordering = ('id',)

    class Meta:
        model = Information


admin.site.register(Project, ProjectAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(Information, InformationAdmin)
