from django.contrib.auth.models import User
from django.db import models


class Project(models.Model):
    name = models.CharField(unique=True, max_length=256, verbose_name='name')

    class Meta:
        managed = True
        db_table = "project"
        verbose_name = "project"
        verbose_name_plural = "projects"

    def __str__(self):
        return self.name


class Link(models.Model):
    link = models.CharField(max_length=1024, verbose_name='link')
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='project')

    class Meta:
        managed = True
        db_table = "links"
        verbose_name = "Link"
        verbose_name_plural = "Links"

    def __str__(self):
        return self.link


class Information(models.Model):
    mail = models.CharField(max_length=256, verbose_name='mail')
    password = models.CharField(max_length=256, verbose_name='password')
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='project')

    class Meta:
        managed = True
        db_table = "Information"
        verbose_name = "Information"
        verbose_name_plural = "Informations"
